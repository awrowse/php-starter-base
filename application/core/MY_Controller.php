<?php

class MY_Controller extends CI_Controller {

    function __construct()
    {
        parent::__construct();
    }
    
    public function enforceAdminAccess(){
        
        if(!$this->session->userdata('user')){
            
            $user = MO\v1\UserQuery::create()->findOneByUsername($_SERVER['PHP_AUTH_USER']);
            if($user && $user->getPassword() == $_SERVER['PHP_AUTH_PW']){
                $this->session->set_userdata('user', 'authenticated');
            } else {
                header('WWW-Authenticate: Basic realm="Admin"');
                header('HTTP/1.0 401 Unauthorized');
                echo 'Invalid Password';
                exit;
            }
            
            
        }
    }
}