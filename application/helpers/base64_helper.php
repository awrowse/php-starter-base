<?php

if ( ! function_exists('urlsafe_base64_encode'))
{
	function urlsafe_base64_encode($string)
	{
            $data = base64_encode($string);
            $data = str_replace(array('+','/','='),array('-','_',''),$data);
            return $data;
	}
        
        function urlsafe_base64_decode($string)
	{
            $data = str_replace(array('-','_'),array('+','/'),$string);
            $mod4 = strlen($data) % 4;
            if ($mod4) {
                $data .= substr('====', $mod4);
            }
            return base64_decode($data);
	}
}