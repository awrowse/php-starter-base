<?php

class CI_Propel
{
  public function CI_Propel()
  {   
    // Include the main Propel script
    require_once  realpath(BASEPATH . '../library/propel/runtime/lib/Propel.php');

    // Initialize Propel with the runtime configuration
    Propel::init(realpath(BASEPATH . "../database/build/conf/michelleosman2-conf.php"));

    // Add the generated 'classes' directory to the include path
    set_include_path(realpath(BASEPATH . "../database/build/classes") . PATH_SEPARATOR . get_include_path());
  }
}