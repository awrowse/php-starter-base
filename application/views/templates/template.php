<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>XXXXX</title>
        <meta name="description" content="XXXXX">
        <!--[if lt IE 9]>
          <script src="//html5shiv.googlecode.com/svn/trunk/html5.js" type="text/javascript"></script>
        <![endif]-->
        <link rel="stylesheet" href="/bootstrap/css/bootstrap.css" />
        <link rel="stylesheet" href="/css/layout.css"/>
        <script src="//code.jquery.com/jquery-latest.js"></script>
        <!-- <script src="/js/jquery-1.7.2.min.js"></script> -->             
    </head>
    <body> 
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container-fluid">
                    <!-- .btn-navbar is used as the toggle for collapsed navbar content -->
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>

                    <a class="brand" href="/">
                        Home
                    </a>
                    
                    
                    <div class="nav-collapse">
                        <ul class="nav pull-right">
                            
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div id="layout_Container">
                <div class="row">
                    <div class="span12">
                        <h1><?php echo $this->title['page']; ?></h1>
                    </div>
                </div>
                
                <div class="row">
                    <div class="span12">
                        <div id="layout_CenterColumn">

                            <div id="layout_Main">
                                <?php print $content; ?>
                            </div>

                        </div>    
                    </div>
                </div>
                <div class="row">
                    <div class="span12">
                        <div id="layout_Footer">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        

        <script src="/bootstrap/js/bootstrap.min.js"></script>

    </body>
</html>