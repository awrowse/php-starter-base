<?php if($this->session->flashdata('status')): ?>
    <div class="alert alert-success">
        <?php echo $this->session->flashdata('status'); ?>
    </div>
<?php endif; ?>
<?php if($this->session->flashdata('warning')): ?>
    <div class="alert">
        <?php echo $this->session->flashdata('warning'); ?>
    </div>          
<?php endif; ?>