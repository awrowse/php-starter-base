<?php
    if($bean->getValidationFailures()):
        $errors = $bean->getValidationFailures();?>
        <div class="alert alert-error">
        <h4>Error!</h4>
        <?php foreach($errors as $error): ?>
            <?php echo $error->getMessage(); ?><br/>

        <?php endforeach; ?>
        </div>
<?php endif; ?>