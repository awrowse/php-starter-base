<?php
ini_set('display_errors',1);
error_reporting(E_ALL ^ E_NOTICE);

date_default_timezone_set('America/Denver'); 

// Include the main Propel script
require_once  realpath('../library/propel/runtime/lib/Propel.php');

// Initialize Propel with the runtime configuration
Propel::init(realpath("../database/build/conf/michelleosman2-conf.php"));

// Add the generated 'classes' directory to the include path
set_include_path(realpath("../database/build/classes") . PATH_SEPARATOR . get_include_path());


/*
 * Medium Import
 * 
 */

$csv_path = realpath('../database/xxxx.csv');
$fp = fopen($csv_path, 'r');

//Get Headers
$header = fgetcsv($fp);

//Get Data Rows
while($row = fgetcsv($fp)){
    $data = array_combine($header, $row);
    

}


fclose($fp);


